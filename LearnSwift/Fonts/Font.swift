//
//  Font.swift
//  LearnSwift
//
//  Created by P21_0079 on 07/08/24.
//

import SwiftUI

extension Font {
    static func getAllFont() {
        for familyname in UIFont.familyNames {
            let fontName = UIFont.fontNames(forFamilyName: familyname)
            print("Familyname: \(familyname) -- Fontname: \(fontName)")
        }
    }

    static func FiraCodeRegular(size: CGFloat) -> Font {
        return Font.custom("FiraCode-Regular", size: size)
    }

    static func PoppinsRegular(size: CGFloat) -> Font {
        return Font.custom("Poppins-Regular", size: size)
    }
}
