import SwiftUI

struct CityListView: View {
    @State private var showFavoriteOnly = false
    @EnvironmentObject private var modelData: ModelData
    @State private var isShowingAlert: Bool = false

    var filteredCities: [Landmark] {
        modelData.landmarks.filter { city in
            !showFavoriteOnly || city.isFavorite
        }
    }

    var body: some View {
        NavigationSplitView {
            List {
                Toggle(isOn: $showFavoriteOnly.animation()) {
                    Text("Favorites only").font(.title2)
                }

                ForEach(filteredCities, id: \.id) { landmark in
                    NavigationLink {
                        CityDetails(landmark: landmark)
                    } label: {
                        CityRow(landmark: landmark)
                    }
                }
                .onDelete(perform: { indexSet in
                    modelData.landmarks.remove(atOffsets: indexSet)
                })
            }
            .navigationTitle("Cities")
            .scrollIndicators(ScrollIndicatorVisibility.hidden)
            .toolbar {
                ToolbarItem {
                    Button("delete", systemImage: "trash") {
                        isShowingAlert = true
                    }
                }
            }
            .alert("Are you sure want to to delete all landmarks", isPresented: $isShowingAlert) {
                Button("OK", role: .destructive) {
                    modelData.landmarks = []
                }
                Button("Cancel", role: .cancel) {}
            }
        } detail: {
            Text("Select a city")
        }
    }

    func delete(at offsets: IndexSet) {
        modelData.landmarks.remove(atOffsets: offsets)
    }
}

#Preview {
    CityListView()
        .environmentObject(ModelData())
}
