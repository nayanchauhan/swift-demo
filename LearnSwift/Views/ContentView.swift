import SwiftUI

struct ContentView: View {
    @State private var selectedTab: Tab = .featured

    @StateObject private var modelData = ModelData()

    var body: some View {
        TabView(selection: $selectedTab,
                content: {
                    CategoryHome()
                        .tabItem {
                            Label("Featured", systemImage: "star")
                        }
                        .tag(Tab.featured)

                    CityListView()
                        .tabItem {
                            Label("List", systemImage: "list.bullet")
                        }
                        .tag(Tab.list)
                })
                .environmentObject(modelData)
    }
}

#Preview {
    ContentView().environmentObject(ModelData())
}
