import SwiftUI

struct CityDetails: View {
//    @Environment(ModelData.self)
//    var modelData
    
    @EnvironmentObject var modelData: ModelData
    
    var landmark: Landmark
    
    var cityIndex: Int {
        modelData.landmarks.firstIndex(where: { $0.id == landmark.id })!
    }
    
    var body: some View {
        @ObservedObject var modelData = modelData
        
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .leading, spacing: 20, content: {
                HStack(content: {
                    Text(landmark.name).font(.largeTitle)
                    
                    Spacer()
                    
                    FavoriteButton(isSet: $modelData.landmarks[cityIndex].isFavorite)
                })
                
                HStack(alignment: .center, content: {
                    Spacer()
                    
                    landmark.image
                        .resizable()
                        .frame(width: 200, height: 200)
                        .cornerRadius(100)
                    
                    Spacer()
                })
                
                HStack {
                    Text("State")
                        .font(.title)
                    
                    Spacer()
                    
                    Text(landmark.state)
                        .font(.title)
                        .foregroundStyle(.gray)
                }
                
                Divider()
                
                VStack(alignment: .leading, content: {
                    Text("About \(landmark.name)").font(.title)
                    Text(landmark.description)
                        .font(.title2)
                        .foregroundStyle(.gray)
                })
            }).padding()
            Spacer()
        }
    }
}

#Preview {
    CityDetails(landmark: ModelData().landmarks[0]).environmentObject(ModelData())
}
