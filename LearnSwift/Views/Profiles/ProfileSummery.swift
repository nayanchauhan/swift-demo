//
//  ProfileSummery.swift
//  LearnSwift
//
//  Created by P21_0079 on 04/07/24.
//

import SwiftUI

struct ProfileSummery: View {
    
    var profile : Profile
    
    var body: some View {
        VStack {
            Text(profile.username)
            Text("Notifications : \(profile.prefersNotifications ? "On" : "Off")")
            Text("Seasonal Photos: \(profile.seasonalPhoto.rawValue)")
            Text("Goal Date: \(profile.goalDate, style: .date)")
        }
    }
}

#Preview {
    ProfileSummery(profile: Profile.default )
}
