//
//  ProfileHost.swift
//  LearnSwift
//
//  Created by P21_0079 on 04/07/24.
//

import SwiftUI

struct ProfileHost: View {
    @Environment(\.editMode)
    var editMode
//    @Environment(ModelData.self)
//    var modelData
    
    @StateObject var modelData = ModelData()
    @State private var draftProfile = Profile.default
    
    var body: some View {
        VStack {
            HStack {
                if editMode?.wrappedValue == .active {
                    Button("Cancel", role: .cancel) {
                        draftProfile = modelData.profile
                        editMode?.animation().wrappedValue = .inactive
                    }
                }
                
                Spacer()
                
                Button(editMode?.animation().wrappedValue == .inactive ? "Edit" : "Done") {
                    if editMode?.animation().wrappedValue == .inactive {
                        editMode?.animation().wrappedValue = .active
                    } else {
                        editMode?.animation().wrappedValue = .inactive
                    }
                }
            }
            
            Spacer()
            
            if editMode?.wrappedValue == .inactive {
                ProfileSummery(profile: modelData.profile)
            } else {
                ProfileEditor(profile: $draftProfile)
                    .onAppear {
                        draftProfile = modelData.profile
                    }
                    .onDisappear {
                        modelData.profile = draftProfile
                    }
            }
            
            Spacer()
        }
        .padding()
    }
}

#Preview {
    ProfileHost()
        .environmentObject(ModelData())
}
