//
//  ProfileEditor.swift
//  LearnSwift
//
//  Created by P21_0079 on 04/07/24.
//

import SwiftUI

struct ProfileEditor: View {
    
    @Binding var profile : Profile
    
    var dateRange : ClosedRange<Date>{
        let minDate = Calendar.current.date(byAdding: .year, value: -1, to: profile.goalDate)!
        let maxDate = Calendar.current.date(byAdding: .year, value: 1, to: profile.goalDate)!
        return minDate...maxDate
    }
    
    var body: some View {
        List {
            HStack {
                Text("Username")
                Spacer()
                TextField("username", text: $profile.username)
                    .foregroundStyle(.secondary)
                    .multilineTextAlignment(.trailing)
            }
            
            Toggle(isOn: $profile.prefersNotifications){
                Text("Enable Notifications")
            }
            
            
            Picker("Seasonal Image",selection: $profile.seasonalPhoto) {
                ForEach(Profile.Season.allCases){ season in
                    Text(season.rawValue).tag(season)
                }
            }
            
            DatePicker(selection: $profile.goalDate, in: dateRange, displayedComponents: .date) {
                Text("Goal Date")
            }
            
        }
    }
}

#Preview {
    ProfileEditor(profile: .constant(.default))
}
