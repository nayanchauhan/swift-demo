//
//  Categories.swift
//  LearnSwift
//
//  Created by P21_0079 on 03/07/24.
//

import SwiftUI

struct CategoryHome: View {
    @EnvironmentObject var modelData: ModelData
    @State var isProfileVisible: Bool = false

    var body: some View {
        NavigationSplitView {
            List {
                modelData.features[0].image
                    .resizable()
                    .frame(height: 200)
                    .clipped()
                    .listRowInsets(EdgeInsets())

                ForEach(modelData.categories.keys.sorted(), id: \.self) { key in
                    CategoryRow(categoryName: key, landmarks: modelData.categories[key]!)
                }
                .listRowInsets(EdgeInsets())
            }
            .scrollIndicators(ScrollIndicatorVisibility.hidden)
            .listStyle(.inset)
            .navigationTitle("Featured")
            .toolbar {
                Button {
                    isProfileVisible.toggle()
                } label: {
                    Label("User Profile", systemImage: "person.crop.circle")
                }
            }
            .sheet(isPresented: $isProfileVisible, content: {
                ProfileHost()
            })
        } detail: {
            Text("Select a landmark")
        }
    }
}

#Preview {
    CategoryHome()
        .environmentObject(ModelData())
}
