import SwiftUI

@main
struct learn_swiftApp: App {
    @StateObject var modelData = ModelData()
    @StateObject var biometric = BiometricModel()

    let dataController = DataController()

    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(modelData)
//            OtherDemo()
//            GestureEvents()
//            HapticFeedback()
//            LoadVideoAsynchronusly()

//            BookList()
//                .environment(\.managedObjectContext, dataController.container.viewContext)
//            FileManagerDemo()
//                NetworkingDemo()
//                LocalNotificationDemo()
//            BiometricAuthentication()
//                .environmentObject(biometric)
//                .environmentObject(modelData)
//            MapDemo()
//            EnviromentObjDemo()
        }
    }
}
