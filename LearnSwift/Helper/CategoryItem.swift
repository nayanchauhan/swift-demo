//
//  CategoryItem.swift
//  LearnSwift
//
//  Created by P21_0079 on 03/07/24.
//

import SwiftUI

struct CategoryItem: View {
    
    let landmark : Landmark
    
    var body: some View {
        VStack(alignment : .leading, spacing: 10) {
            landmark.image
                .renderingMode(.original)
                .resizable()
                .frame(width: 155, height: 155)
                .cornerRadius(7)
            
            Text(landmark.name)
                .foregroundStyle(.black)
                .font(.title3)
            
        }.padding(15)
        
    }
}

#Preview {
    CategoryItem(landmark: ModelData().landmarks[0])
}
