//
//  TestCommonButton.swift
//  LearnSwift
//
//  Created by P21_0079 on 07/10/24.
//

import SwiftUI

struct TestCommonButton: View {
    var label: String
    var action: () -> Void

    var body: some View {
        Button(label, action: action)
        #if os(macOS)
            .buttonStyle(.borderedProminent)
        #else
            .buttonStyle(TestCommonButtonStyles())
        #endif
    }
}

#if os(iOS)
    struct TestCommonButtonStyles: ButtonStyle {
        @Environment(\.isEnabled) var isEnabled

        func makeBody(configuration: Configuration) -> some View {
            configuration.label
                .fontWeight(.bold)
                .frame(maxWidth: .infinity)
                .padding()
                .foregroundStyle(.background)
                .background(
                    RoundedRectangle(cornerRadius: 12)
                        .fill(isEnabled ? .purple : .gray.opacity(0.6))
                        .scaleEffect(configuration.isPressed ? 0.97 : 1)
                        .opacity(configuration.isPressed ? 0.7 : 1)
                        .animation(.easeInOut, value: configuration.isPressed)
                )
        }
    }
#endif

#Preview {
    TestCommonButton(label: "Test") {}
}
