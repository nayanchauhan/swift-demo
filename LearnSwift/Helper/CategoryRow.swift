//
//  CategoryRow.swift
//  LearnSwift
//
//  Created by P21_0079 on 03/07/24.
//

import SwiftUI

struct CategoryRow: View {
    
    var categoryName : String
    var landmarks : [Landmark]
    
    var body: some View {
        Text(categoryName)
            .font(.headline)
            .padding([.leading], 15)
        
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(alignment: .top, spacing: 0) {
                ForEach(landmarks) { landmark in
                    NavigationLink {
                        CityDetails(landmark: landmark)
                    } label: {
                        CategoryItem(landmark: landmark)
                    }
                }
            }
        }
        
        
    }
}

#Preview {
    let landmarks = ModelData().landmarks
    return CategoryRow(categoryName: landmarks[0].category.rawValue, landmarks: Array(landmarks.prefix(5))
    )
}
