import SwiftUI

struct CityRow: View {
    var landmark : Landmark
    
    var body: some View {
        HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/,spacing: 15, content: {
            landmark.image
                .resizable().background(.white)
                .frame(width : 50, height:50)
                .cornerRadius(10)
            
            VStack {
                Text(landmark.name)
                    .bold()
                    .lineLimit(1)
                    .font(.title2)
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                Text(landmark.description)
                    .lineLimit(1)
            }
            
            Spacer()
            
            if landmark.isFavorite {
                Image(systemName: "star.fill")
                    .foregroundStyle(.yellow)
            }
            
        })
        
    }
}

#Preview {
    let cities = ModelData().landmarks
    return Group{
        CityRow(landmark: cities[0])
    }
}
