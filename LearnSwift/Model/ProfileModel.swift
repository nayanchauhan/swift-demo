//
//  ProfileModel.swift
//  LearnSwift
//
//  Created by P21_0079 on 04/07/24.
//

import Foundation

struct Profile {
    var username : String
    var prefersNotifications : Bool = true
    var seasonalPhoto : Season = Season.winter
    var goalDate : Date  = Date()
    
    static let `default` = Profile(username: "john_doe")
    
    enum Season : String, CaseIterable, Identifiable {
        case spring = "🌷"
        case summer = "🌞"
        case autumn = "🍂"
        case winter = "☃️"
        
        var id: String { rawValue }
    }
}
