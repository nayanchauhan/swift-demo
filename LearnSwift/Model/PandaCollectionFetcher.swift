//
//  PandaCollectionFetcher.swift
//  LearnSwift
//
//  Created by P21_0079 on 08/07/24.
//

import Foundation

struct PandaModel : Codable{
    var description : String
    var imageUrl : URL?
    
    static let defaultPanda = PandaModel(description: "Cute Panda",
                                         imageUrl: URL(string: "https://assets.devpubs.apple.com/playgrounds/_assets/pandas/pandaBuggingOut.jpg"))
}

struct PandaCollection :Decodable{
    var sample : [PandaModel]
}

class PandaCollectionFetcher {
    let baseUrl = "http://playgrounds-cdn.apple.com/assets/pandaData.json"
    @Published var responseData = PandaCollection(sample: [PandaModel.defaultPanda])
    
    func fetchData() async
    throws {
        guard let url = URL(string:baseUrl ) else { return }
        
        let (data, response) = try await URLSession.shared.data(for: URLRequest(url: url))
        guard (response as? HTTPURLResponse)?.statusCode == 200 else {
            throw FetchError.BadRequest
        }
        
        Task { @MainActor in
            responseData = try JSONDecoder().decode(PandaCollection.self, from: data)
        }
    }
}
