//
//  constant.swift
//  LearnSwift
//
//  Created by P21_0079 on 01/07/24.
//

import Foundation

// ------------------ ENUMS ---------------------- //

enum Tab {
    case featured
    case list
}

@objc protocol TestProtocol {
    init(testPrameter: Int)
    @objc optional var optionalStringVariable: String {
        get
    }
}

// Protocol demo with class
class BaseClass: TestProtocol {
    var testClassParam: Int

    required init(testPrameter: Int) {
        self.testClassParam = testPrameter
    }
}

let classInstense = BaseClass(testPrameter: 501)

// Protocol demo with Structure ::: @objc is not available with enums and structures. to use with structure remove @objc from protocol.

// struct TestStructure : TestProtocol {
//
//    var param : Int
//
//    init(testPrameter: Int) {
//        self.param = testPrameter
//    }
// }
// let getTestParameterFromStructure = TestStructure(testPrameter: 200)

enum LearnSwift {
    enum ImageName {
        static var defaultImageFrame: String = "default-image-frame"
    }
}
