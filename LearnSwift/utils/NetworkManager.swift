//
//  NetworkManager.swift
//  LearnSwift
//
//  Created by P21_0079 on 05/09/24.
//

import Foundation
import Network

// class NetworkMonitor: ObservableObject {
//    let monitor = NWPathMonitor()
//    let queue = DispatchQueue(label: "NetworkManager")
//    @Published var isConnected = true
//
//    var imageName: String {
//        return isConnected ? "wifi" : "wifi.slash"
//    }
//
//    var connectionDescription: String {
//        if isConnected {
//            return "Internet connection looks good!"
//        } else {
//            return "It looks like you're not connected to the internet. Make sure WiFi / Mobile Data is enabled and try again"
//        }
//    }
//
//    init() {
//        monitor.pathUpdateHandler = { path in
//            DispatchQueue.main.async {
//                self.isConnected = path.status == .satisfied
//                Task {
//                    await MainActor.run {
//                        self.objectWillChange.send()
//                    }
//                }
//            }
//        }
//        monitor.start(queue: queue)
//    }
// }

class NetworkMonitor: ObservableObject {
    private let monitor = NWPathMonitor()
    private let queue = DispatchQueue(label: "NetworkMonitor")
    var isConnected: Bool = false

    init() {
        monitor.pathUpdateHandler = { path in
            DispatchQueue.main.async {
                self.isConnected = path.status == .satisfied
                Task {
                    await MainActor.run {
                        self.objectWillChange.send()
                    }
                }
            }
        }
        monitor.start(queue: queue)
    }
}
