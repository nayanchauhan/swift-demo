//
//  BookModel.swift
//  LearnSwift
//
//  Created by P21_0079 on 16/09/24.
//

import CoreData
import Foundation

class DataController: ObservableObject {
    var container = NSPersistentContainer(name: "CoreDataModel")

    init() {
        self.container.loadPersistentStores { _, error in
            if let error = error {
                print("Core data failed to load. \(error.localizedDescription)")
                return
            }

            // merge duplicate objects
            self.container.viewContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        }
    }
}
