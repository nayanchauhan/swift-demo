//
//  OtherDemo.swift
//  LearnSwift
//
//  Created by P21_0079 on 22/08/24.
//

import PhotosUI
import SwiftUI

struct OtherDemo: View {
    @State private var sliderValue: Double
    @State private var isSwitchOn: Bool
    @State private var selectedColor: Int
    @State private var selectedDate: Date
    @State private var dates: Set<DateComponents>
    @State private var selectedColorValue: Color
    @State private var brightnessLevel: Float
    @State private var timePercentage: Float
    let timerValue = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @State private var isVibrateOnRing: Bool
    @State private var selectedPhotos: [PhotosPickerItem]

    static let tempValue = Measurement(value: 8, unit: UnitLength.feet)

    let dateRange: ClosedRange<Date> = {
        let calender = Calendar.current
        let startDateComponent = DateComponents(year: 1999, month: 1, day: 1)
        let endDateComponent = DateComponents(year: 2002, month: 12, day: 31)
        return calender.date(from: startDateComponent)! ... calender.date(from: endDateComponent)!
    }()

    init(
        sliderValue: Double = 50.0,
        isSwitchOn: Bool = false,
        selectedColor: Int = 1,
        selectedDate: Date = .now,
        dates: Set<DateComponents> = [],
        selectedColorValue: Color = .cyan,
        brightnessLevel: Float = 40,
        timePercentage: Float = 1,
        isVibrateOnRing: Bool = false,
        selectedPhotos: [PhotosPickerItem] = []
    ) {
        self.sliderValue = sliderValue
        self.isSwitchOn = isSwitchOn
        self.selectedColor = selectedColor
        self.selectedDate = selectedDate
        self.dates = dates
        self.selectedColorValue = selectedColorValue
        self.brightnessLevel = brightnessLevel
        self.timePercentage = timePercentage
        self.isVibrateOnRing = isVibrateOnRing
        self.selectedPhotos = selectedPhotos
    }

    var body: some View {
        NavigationStack {
            VStack {
                Form {
                    Section("custom swipe actions") {
                        List {
                            Text("Swipe me left or right!")
                                .swipeActions {
                                    Button {}
                                        label: {
                                            Label("View", systemImage: "eye.fill")
                                        }
                                        .tint(.blue)
                                }
                                .swipeActions(edge: .leading) {
                                    Button {}
                                        label: {
                                            Label("Pin", systemImage: "pin")
                                        }
                                        .tint(.green)
                                }
                                .swipeActions {
                                    Button {}
                                        label: {
                                            Label("Delete", systemImage: "minus.circle.fill")
                                        }
                                        .tint(.red)
                                }
                        }
                    }

                    Section {
                        PhotosPicker(selection: $selectedPhotos, maxSelectionCount: 3) {
                            Label("Upload Image", systemImage: "square.and.arrow.up")
                        }
                        .onChange(of: selectedPhotos, perform: { _ in
                            print("selectedPhotos", selectedPhotos)
//                            print("newValue", newValue)
                        })
                        .task {
                            PHPhotoLibrary.requestAuthorization(for: .readWrite) { status in
                                switch status {
                                    case .authorized: print("status1", status.rawValue)
                                    case .notDetermined: print("status2", status.rawValue)
                                    case .restricted: print("status3", status.rawValue)
                                    case .denied: print("status4", status.rawValue)
                                    case .limited: print("status5", status.rawValue)
                                    @unknown default: print("status6", status.rawValue)
                                }
                            }
                        }
                    }

                    // MARK: - Slider Demo

                    Section(header: Text("Slider Demo")) {
                        Slider(value: $sliderValue, in: 0 ... 101, step: 25)
                        Text("Slider value is : \(sliderValue.formatted())")

                        Picker(selection: $selectedColor, label: Text("Favorite Color")) {
                            Text("Red").tag(1)
                            Text("Green").tag(2)
                            Text("Blue").tag(3)
                            Text("Other").tag(4)
                        }

                        Toggle(isOn: $isVibrateOnRing) {
                            Text("Vibrate on Ring")
                            Text("Enable vibration when the phone rings")
                        }

                        Toggle("Vibrate on Ring",
                               systemImage: "dot.radiowaves.left.and.right",
                               isOn: $isSwitchOn)

                        Text("hello")
                        Text(Self.tempValue.formatted())
                        Text(NetworkMonitor().isConnected ? "Connected" : "Network not available")
                    }

                    Menu {
                        Button(action: {}) {
                            Label("Add to Reading List", systemImage: "eyeglasses")
                        }
                        Button(action: {}) {
                            Label("Add Bookmarks for All Tabs", systemImage: "book")
                        }
                        Button(action: {}) {
                            Label("Show All Bookmarks", systemImage: "books.vertical")
                        }
                    } label: {
                        Label("Add Bookmark", systemImage: "book")
                    }
                    #if os(macOS)
                    .menuStyle(.borderlessButton)
                    #endif
                    .menuStyle(.button)

                    // show when content not available
//                    ContentUnavailableView {
//                        Label("No Mail", systemImage: "tray.fill")
//                    } description: {
//                        Text("New mails you receive will appear here.")
//                    }

                    // Default Content Unavailable View for serch text
//                    ContentUnavailableView.search

                    // MARK: - Gauage Demo, Progress Demo

                    Section {
                        ProgressView(value: timePercentage, total: 100)

                        Text("\(timePercentage.formatted())% Uploaded ")

                        Gauge(value: brightnessLevel, in: 1 ... 101) {
                            Text("Guage")
                        }
                        .gaugeStyle(.automatic)
                    }

                    // MARK: - Color picker demo

                    Section {
                        ColorPicker("Select color", selection: $selectedColorValue)
                    } header: {
                        Text("Color Picker demo")
                    }

                    // MARK: - Date Picker Demo

                    Section(header: Text("Date Picker Demo")) {
                        DatePicker(selection: $selectedDate, in: dateRange, displayedComponents: [.date]) {
                            Text("Birth Date")
                            Text("Select Birth Date")
                        }
                        // change date picker style
                        .datePickerStyle(.graphical)

//                        MultiDatePicker("Dates", selection: $dates)
                    }

                    // MARK: - Share link demo

                    Section {
                        ShareLink(item: URL(string: "https://www.instagram.com")!)
                    }
                }
            }
            .navigationTitle("Other Demo")
            .onReceive(timerValue) { _ in
                if timePercentage == 100 {
                    timerValue.upstream.connect().cancel()
                } else {
                    timePercentage += 1
                }
            }
        }
//        .sensoryFeedback(.selection, trigger: isVibrateOnRing)
    }
}

#Preview {
    OtherDemo()
}
