import PhotosUI
import SwiftUI

struct AddBook: View {
    @State private var title: String
    @State private var author: String
    @State private var genre: String
    @State private var rating: Int
    @State private var review: String
    @State private var isSaveButtonDisabled: Bool = true
    @State private var selectedItems: [PhotosPickerItem] = []
    @State private var selectedImages: [Image] = []

    var book: Book?

    init(book: Book? = nil, genre: String = "") {
        self.book = book
        self.title = book?.title ?? ""
        self.genre = book?.genre ?? "Kids"
        self.author = book?.author ?? ""
        self.rating = Int(book?.rating ?? 0)
        self.review = book?.review ?? ""
    }

    let geners: [String] = ["Romantic", "Funny", "Horror", "Science Friction", "Kids", "Thriller", "Romance", "Mystery", "Biography"]

    @Environment(\.managedObjectContext) var moc
    @Environment(\.dismiss) var dismiss

    var body: some View {
        NavigationStack {
            VStack {
                Form {
                    // MARK: Book Details

                    Section {
                        TextField("Book Title", text: $title)
                        TextField("Author", text: $author)
                        Picker("Select Genre", selection: $genre) {
                            ForEach(geners, id: \.self) { genre in
                                Text(genre)
                                    .tag(genre)
                            }
                        }
                    } header: {
                        Text("Book details")
                    }

                    // MARK: Upload book photos

                    Section {
                        if selectedImages.count > 0 {
                            ForEach(0 ..< selectedImages.count, id: \.self) { index in
                                ZStack(alignment: .topTrailing) {
                                    selectedImages[index]
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .clipShape(RoundedRectangle(cornerRadius: 10))

                                    Image(systemName: "xmark")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: 13, height: 13)
                                        .padding(8)
                                        .foregroundStyle(.primary)
                                        .background(.ultraThinMaterial)
                                        .clipShape(Circle())
                                        .shadow(radius: 2)
                                        .offset(x: -10, y: 10)
                                        .onTapGesture {
                                            selectedItems.remove(at: index)
                                            selectedImages.remove(at: index)
                                        }
                                }
                            }

                        } else {
                            PhotosPicker(selection: $selectedItems, maxSelectionCount: 3) {
                                HStack {
                                    Spacer()
                                    VStack {
                                        Image(systemName: "icloud.and.arrow.up")
                                            .resizable()
                                            .foregroundStyle(.blue)
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: 50, height: 50)

                                        Text("Select Images")
                                    }
                                    .padding(.vertical, 30)
                                    Spacer()
                                }
                            }
                            .onChange(of: selectedItems, perform: { _ in
                                selectedImages.removeAll()
                                Task {
                                    for item in selectedItems {
                                        if let image = try? await item.loadTransferable(type: Image.self) {
                                            selectedImages.append(image)
                                        }
                                    }
                                }
                            })
                        }
                    } header: {
                        Text("Upload book images")
                    }

                    // MARK: Review Section

                    Section {
                        VStack {
                            TextField("Write a review", text: $review, axis: .vertical)
                                .lineLimit(4 ... 7)

                            Divider()

                            HStack {
                                Text("Select rating")
                                Spacer()
                                RatingView(rating: $rating, starSize: 27)
                            }
                        }
                    } header: {
                        Text("Write a review")
                    }

                    // MARK: Save Button

                    Section {
                        HStack {
                            Spacer()
                            Button("Save") {
                                let newBook = Book(context: moc)
                                newBook.id = UUID()
                                newBook.title = title
                                newBook.author = author
                                newBook.genre = genre
                                newBook.review = review
                                if rating > 5 {
                                    newBook.rating = Int16(5)
                                } else {
                                    newBook.rating = Int16(rating)
                                }

                                try? moc.save()
                                dismiss()
                            }
                            .disabled(title.isEmpty || author.isEmpty || genre.isEmpty || review.isEmpty || rating < 1)
                            Spacer()
                        }
                    }
                }
            }
            .navigationTitle("Add Book")
        }
    }
}

#Preview {
    AddBook()
}
