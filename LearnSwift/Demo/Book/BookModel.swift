//
//  BookModel.swift
//  LearnSwift
//
//  Created by P21_0079 on 18/09/24.
//

import CoreData
import Foundation

class BookModel: NSObject, NSFetchRequestResult {
    var id: UUID = .init()
    var title: String?
    var author: String?
    var rating: Int?
    var genre: String = ""
    var review: String?
}
