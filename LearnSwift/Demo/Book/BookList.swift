//
//  BookView.swift
//  LearnSwift
//
//  Created by P21_0079 on 16/09/24.
//

import CoreData
import SwiftUI

struct BookList: View {
    @FetchRequest(sortDescriptors: []) var books: FetchedResults<Book>
//    var tests: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Test")
    @State private var isSheetOpen: Bool = false
    @FetchRequest(sortDescriptors: []) var tests: FetchedResults<Test>
    
    @Environment(\.managedObjectContext) var moc
    
    var body: some View {
        NavigationStack {
            VStack {
                List(books) { book in
                    NavigationLink {
                        BookDetail(book: book)
                    } label: {
                        VStack(alignment: .leading) {
                            HStack {
                                Text(book.title ?? "")
                                    .font(.title2)
                            
                                Spacer()
                            
                                Menu {
                                    NavigationLink { BookDetail(book: book) }
                                        label: {
                                            Label("View Details", systemImage: "eye")
                                        }
                                    
                                    NavigationLink {
                                        AddBook(book: book)
                                    }
                                    label: {
                                        Label("Edit", systemImage: "square.and.pencil")
                                    }
                 
                                    Button("Delete", systemImage: "trash") {
                                        moc.delete(book)
                                        try? moc.save()
                                    }
                                    .labelStyle(.iconOnly)
                                
                                } label: {
                                    Label("", systemImage: "ellipsis")
                                        .labelStyle(.iconOnly)
                                        .rotationEffect(.degrees(90))
                                        .frame(width: 30, height: 25)
                                }
                            }
                            Divider()
                        
                            Text(book.review ?? "No review available")
                                .foregroundStyle(.secondary)
                                .lineLimit(3)
                        
                            HStack {
                                Spacer()
                                Text((book.author != nil) ? "~ \(book.author!)" : "Unknown")
                                    .font(.headline)
                                    .foregroundStyle(.secondary)
                            }
                        }
                    }
                }
                .listRowSpacing(25)
                .overlay {
                    if books.count == 0 {
                        Form {
                            BooksUnavailableView()
                        }
                    }
                }
            }
            .navigationTitle("Books")
            .toolbar {
                ToolbarItem(placement: .topBarTrailing) {
                    Button("sdf", systemImage: "plus") {
                        isSheetOpen = true
                    }
                }
            }
            .sheet(isPresented: $isSheetOpen) {
                AddBook()
            }
        }
    }
}

#Preview {
    BookList()
}
