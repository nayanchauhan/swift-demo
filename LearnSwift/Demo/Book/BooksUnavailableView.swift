//
//  BooksNotAvailable.swift
//  LearnSwift
//
//  Created by P21_0079 on 17/09/24.
//

import SwiftUI

struct BooksUnavailableView: View {
    var body: some View {
        VStack {
//            ContentUnavailableView {
//                Label("No Books available", systemImage: "book")
//            } description: {
//                Text("Added books by clicking on plus(+) icon will be appear here.")
//            }
            Form {
                Label("No Books available", systemImage: "book")
                Text("Added books by clicking on plus(+) icon will be appear here.")
            }
        }
    }
}

#Preview {
    BooksUnavailableView()
}
