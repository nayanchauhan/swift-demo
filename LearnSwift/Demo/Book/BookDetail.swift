//
//  BookDetail.swift
//  LearnSwift
//
//  Created by P21_0079 on 18/09/24.
//

import SwiftUI

struct BookDetail: View {
    let book: Book

    var body: some View {
        NavigationStack {
            Form {
                VStack {
                    ZStack(alignment: .bottomTrailing) {
                        Image(String(Int.random(in: 1 ..< 13)))
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .clipShape(.rect(topLeadingRadius: 20, topTrailingRadius: 20))
                            .opacity(0.8)

                        Text(book.genre ?? "Unknown genre")
                            .padding(EdgeInsets(top: 2, leading: 12, bottom: 2, trailing: 12))
                            .background(.ultraThinMaterial.opacity(0.7))
                            .clipShape(Capsule())
                            .foregroundStyle(.primary)
                            .font(.subheadline)
                            .textCase(.uppercase)
                            .shadow(color: .primary.opacity(0.6), radius: 1)
                            .offset(x: -10, y: -10)
                    }
                    Text(book.review ?? "No review available")
                        .foregroundStyle(.secondary)

                    HStack {
                        Spacer()

                        Text(((book.author) != nil) ? "~ \(book.author!)" : "Unknown author")
                            .font(.headline)
                            .foregroundStyle(.secondary)
                    }
                }
                .navigationTitle(book.title ?? "Unknown")
                .navigationBarTitleDisplayMode(.inline)
            }
        }
    }
}

// #Preview {
//    BookDetail(book: )
// }
