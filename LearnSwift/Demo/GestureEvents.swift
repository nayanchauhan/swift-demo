//
//  GestureEvents.swift
//  LearnSwift
//
//  Created by P21_0079 on 28/10/24.
//

import SwiftUI

// drag gesture demo

struct GestureEvents: View {
    @State private var isDragging: Bool = false
    @State private var offset: CGSize = .zero

    var body: some View {
        let longPressGesture = LongPressGesture()
            .onEnded { _ in
                withAnimation {
                    isDragging = true
                }
            }

        let dragGesture = DragGesture()
            .onChanged { value in
                offset = value.translation
            }
            .onEnded { _ in
                withAnimation {
                    offset = .zero
                    isDragging = false
                }
            }

        let combineGesture = longPressGesture.sequenced(before: dragGesture)

        Circle()
            .fill(.green)
            .frame(width: 70, height: 70)
            .scaleEffect(isDragging ? 1.5 : 1)
            .offset(offset)
            .gesture(combineGesture)
    }
}

#Preview {
    GestureEvents()
}
