//
//  NetworkingDemo.swift
//  LearnSwift
//
//  Created by P21_0079 on 07/08/24.
//

import SwiftUI

struct Post: Identifiable, Decodable, Hashable {
    var id: Int
    var userId: Int
    var title: String
    var body: String
}

// endpoint : https://jsonplaceholder.typicode.com/posts

struct NetworkingDemo: View {
    @State private var posts: [Post] = []
    @State private var text: String = ""

    @Environment(\.dismiss) var dismiss

    var body: some View {
        NavigationStack {
            VStack {
                List {
                    ForEach(posts, id: \.self) { post in
                        VStack(alignment: .leading) {
                            Text(post.title)
                                .font(.title2)
                                .fontWeight(.semibold)

                            Divider()

                            Text(post.body)
                                .lineLimit(2)

                            NavigationLink("Read more") {
                                Form {
                                    VStack(alignment: .leading) {
                                        Image(LearnSwift.ImageName.defaultImageFrame)
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .cornerRadius(15)
                                            .padding(.top, 10)

                                        Text(post.title)
                                            .font(.title2)
                                            .fontWeight(.semibold)

                                        Divider()

                                        Text(post.body)

                                        TextField("Comment", text: $text, axis: .vertical)
                                            .lineLimit(4, reservesSpace: true)
                                            .padding(10)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color.primary.opacity(0.2), lineWidth: 0.3)
                                            )

                                        HStack {
                                            Spacer()
                                            Button("Submit") {
                                                dismiss()
                                            }
                                            .buttonStyle(.borderedProminent)
                                            Spacer()
                                        }

                                        Spacer()
                                    }
                                }
                            }
                        }
                    }
                }
                .navigationTitle("Posts")
                .listRowSpacing(30)
            }
            .task { fetchApiData() }
        }
    }

    private func fetchApiData() {
        let url = URL(string: "https://jsonplaceholder.typicode.com/posts")!

        var request = URLRequest(url: url)

        // set headers
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        // perform task to fetch data from api
        let task = URLSession.shared.dataTask(with: request) { data, _, error in

            // error handling
            if let error = error {
                print("An Error occurred while fetch data", error)
                fatalError("erorr while fetching data from api : \(error)")
            }

            // data handling : request successfull
            guard let data = data else {
                fatalError("erorr data : \(String(describing: data))")
            }

            // Decode data into json
            do {
                let apiData = try JSONDecoder().decode([Post].self, from: data)
                self.posts = apiData

                posts = posts
            } catch let jsonError {
                fatalError("erorr while docode json : \(jsonError)")
            }
        }

        task.resume()
    }
}

#Preview {
    NetworkingDemo()
}
