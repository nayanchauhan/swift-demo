//
//  BiometricAuthentication.swift
//  LearnSwift
//
//  Created by P21_0079 on 15/10/24.
//

import LocalAuthentication
import SwiftUI

struct BiometricAuthentication: View {
    @EnvironmentObject var biometric: BiometricModel

    var body: some View {
        VStack {
            Text(biometric.isAuthenticated ? "Unlocked" : "Locked")

            Button("Unlock Device") {
                biometric.authenticate()
            }
            .buttonStyle(.borderedProminent)
        }
        .padding()
    }
}

#Preview {
    BiometricAuthentication()
}
