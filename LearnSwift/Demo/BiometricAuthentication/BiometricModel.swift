//
//  BiometricModel.swift
//  LearnSwift
//
//  Created by P21_0079 on 15/10/24.
//

import Foundation
import LocalAuthentication

@MainActor class BiometricModel: ObservableObject {
    @Published var isAuthenticated: Bool = false
    private let context = LAContext()
    private var error: NSError?
    private var reason: String = "Please authenticate yourself"

    func authenticate() {
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { success, authenticationError in
                DispatchQueue.main.async {
                    if success {
                        self.isAuthenticated = true
                        print("Authorized successfully")
                    } else {
                        self.isAuthenticated = false
                        print("😡 Error: \(authenticationError?.localizedDescription ?? "Unknown error")")
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                print("Biometric not avialable")
                self.isAuthenticated = true
            }
        }
    }
}
