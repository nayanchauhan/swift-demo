//
//  NavigationDemoView.swift
//  LearnSwift
//
//  Created by P21_0079 on 07/10/24.
//

import SwiftUI

struct NavigationDemoView: View {
    @ObservedObject var navigationModel = NavigationModel()
    @Binding var experience: Experience?

    var body: some View {
        NavigationStack {
            VStack {
                Text("Main View")
            }
            .toolbar {
                ToolbarItem {
                    Button {
                        navigationModel.isSheetPresented = true
                    }
                    label: {
                        Label("", systemImage: "square.3.layers.3d")
                    }
                }
            }
            .sheet(isPresented: $navigationModel.isSheetPresented) {
                NavigationList(experience: $experience)
            }
        }
    }
}

#Preview {
    NavigationDemoView(experience: .constant(.stack))
}
