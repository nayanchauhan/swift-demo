//
//  NavigationModel.swift
//  LearnSwift
//
//  Created by P21_0079 on 07/10/24.
//

import Foundation
import SwiftUI

enum Experience: Int, Identifiable, CaseIterable {
    case stack
    case twoColumn
    case threeColumn

    var id: Int { rawValue }

    var image: String {
        switch self {
            case .stack: return "list.bullet.rectangle.portrait"
            case .twoColumn: return "sidebar.left"
            case .threeColumn: return "rectangle.split.3x1"
        }
    }

    /// String, LocalizedStringKey
    var title: String {
        switch self {
            case .stack: return "Stack"
            case .twoColumn: return "Two Column"
            case .threeColumn: return "Three Column"
        }
    }

    /// String, LocalizedStringKey
    var description: String {
        switch self {
            case .stack:
                return "Presents a stack of views over a root view."
            case .twoColumn:
                return "Presents views in two columns: sidebar and detail."
            case .threeColumn:
                return "Presents views in three columns: sidebar, content, and detail."
        }
    }
}

class NavigationModel: ObservableObject {
    @Published var isSheetPresented: Bool = false
}
