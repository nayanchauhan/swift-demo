//
//  NavigationList.swift
//  LearnSwift
//
//  Created by P21_0079 on 07/10/24.
//

import SwiftUI

struct NavigationList: View {
    @State private var selection: Experience?
    @Binding var experience: Experience?
    @Environment(\.dismiss) var dismiss

    var body: some View {
        NavigationStack {
            VStack(alignment: .leading) {
                Spacer()
                HStack {
                    Spacer()

                    Text("Choose your nagvigation experiance")
                        .font(.largeTitle)
                        .multilineTextAlignment(.center)
                        .lineLimit(2, reservesSpace: true)
                    Spacer()
                }
                Spacer()
                ForEach(Experience.allCases) {
                    NavigationItem(experience: $0, selection: $selection)
                }
                Spacer()
            }
            .safeAreaInset(edge: .bottom) {
                TestCommonButton(label: "Continue") {
                    experience = selection
                    dismiss()
                }
                .disabled(selection == nil)
            }
            .scenePadding()
        }
    }
}

#Preview {
    NavigationList(experience: .constant(.stack))
}
