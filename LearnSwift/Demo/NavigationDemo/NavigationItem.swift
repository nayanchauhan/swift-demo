//
//  NavigationItem.swift
//  LearnSwift
//
//  Created by P21_0079 on 07/10/24.
//

import SwiftUI

struct NavigationItem: View {
    var experience: Experience
    @Binding var selection: Experience?

    var body: some View {
        HStack {
            Image(systemName: experience.image)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 30, height: 30)
                .padding()
                .foregroundStyle(shapeStyle(.purple))

            VStack(alignment: .leading) {
                Text(experience.title)
                    .font(.title2)
                    .fontWeight(.semibold)
                    .lineLimit(2)
                    .foregroundStyle(experience == selection ? AnyShapeStyle(BackgroundStyle()) : AnyShapeStyle(.foreground))

                Text(experience.description)
                    .lineLimit(3)
                    .foregroundStyle(experience == selection ? AnyShapeStyle(BackgroundStyle()) : AnyShapeStyle(.foreground))
            }
        }
        .padding()
        .onTapGesture {
            selection = experience
        }
        .background {
            RoundedRectangle(cornerRadius: 12)
                .fill(experience == selection ? AnyShapeStyle(.purple) : AnyShapeStyle(BackgroundStyle()))
        }
    }

    func shapeStyle<S: ShapeStyle>(_ style: S) -> some ShapeStyle {
        if selection == experience {
            return AnyShapeStyle(.background)
        } else {
            return AnyShapeStyle(style)
        }
    }
}

#Preview {
    NavigationItem(experience: .stack, selection: .constant(.stack))
}
