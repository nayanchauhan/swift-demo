//
//  RatingView.swift
//  LearnSwift
//
//  Created by P21_0079 on 17/09/24.
//

import SwiftUI

struct RatingView: View {
    @Binding var rating: Int
    var maxRating: Int = 5
    var starSize: CGFloat = 20

    var body: some View {
        VStack {
            HStack {
                ForEach(1 ..< maxRating + 1, id: \.self) { number in

                    Image(systemName: "star.fill")
                        .resizable()
                        .onTapGesture { rating = number }
                        .foregroundStyle(rating >= number ? .yellow : .gray.opacity(0.5))
                        .aspectRatio(contentMode: .fit)
                        .frame(width: starSize, height: starSize)
                }
            }
        }
    }
}

#Preview {
    RatingView(rating: .constant(3))
}
