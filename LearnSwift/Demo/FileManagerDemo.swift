//
//  FileManager.swift
//  LearnSwift
//
//  Created by P21_0079 on 23/09/24.
//

import SwiftUI

struct FileManagerDemo: View {
    let fileManger: FileManager = .default

    var body: some View {
        Text("Hello, World!")
            .onAppear {
                let getDocumentDirectory = fileManger.urls(for: .documentDirectory, in: .userDomainMask)
                print("Document Directory Path is -->>", getDocumentDirectory)
            }
    }
}

#Preview {
    FileManagerDemo()
}
