//
//  CoreImageDemo.swift
//  LearnSwift
//
//  Created by P21_0079 on 08/10/24.
//

import CoreImage
import CoreImage.CIFilterBuiltins
import SwiftUI

struct CoreImageDemo: View {
    @State private var image: Image?
    @State private var intensity: Float = 0
    @State private var blurRadius: Float = 0

    var body: some View {
        NavigationStack {
            VStack {
                if image == nil {
                    Text("Unable to load image")

                } else {
                    image?
                        .resizable()
                        .scaledToFit()

                    Form {
                        Section("Intensity") {
                            HStack {
                                Slider(value: $intensity, in: 0 ... 1, step: 0.1) {}
                                Text(String(intensity.formatted(.number.precision(.fractionLength(1)))))
                            }
                        }

                        Section("Blur") {
                            HStack {
                                Slider(value: $blurRadius, in: 0 ... 100, step: 1) {}
                                Text(String(blurRadius))
                            }
                        }
                    }
                }
            }
            .onAppear {
                loadImage()
            }
            .onChange(of: intensity, perform: { _ in
                loadImage()
            })
            .onChange(of: blurRadius, perform: { _ in
                loadImage()
            })
        }
    }

    func loadImage() {
        guard let inputImage = UIImage(named: "silversalmoncreek") else { return }

        // convert image into Core Image
        let beginImage = CIImage(image: inputImage)

        let imageContext = CIContext()

//        // apply filter : sepiaTone
//        let currentFilter = CIFilter.sepiaTone()
//        currentFilter.inputImage = beginImage
//        currentFilter.intensity = intensity

        // filter 2 :
        let secondFilter = CIFilter.bokehBlur()
        secondFilter.inputImage = beginImage
        secondFilter.radius = blurRadius
//        secondFilter.softness = 0.5

        // output image
        guard let outputImage = secondFilter.outputImage else { return }

        // convert image to swiftui image
        if let cgImage = imageContext.createCGImage(outputImage, from: outputImage.extent) {
            let uiImage = UIImage(cgImage: cgImage)
            image = Image(uiImage: uiImage)
        }
    }
}

#Preview {
    CoreImageDemo()
}
