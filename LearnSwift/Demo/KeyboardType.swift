//
//  KeyboardType.swift
//  LearnSwift
//
//  Created by P21_0079 on 22/08/24.
//

import SwiftUI

struct KeyboardType: View {
    @State private var text: String = ""

    var body: some View {
        NavigationStack {
            VStack {
                ScrollView {
                    TextField("Test", text: $text)
                        .padding()
                        .overlay(
                            RoundedRectangle(cornerRadius: 25.0)
                                .stroke(Color.black.opacity(0.3))
                        )
                        .padding()
//                        .textContentType(.username)
//                        .textContentType(.creditCardNumber)
                        .textInputAutocapitalization(.characters)

//                    .keyboardType(.emailAddress)
//                    .keyboardType(.URL)
//                    .keyboardType(.asciiCapable)
//                    .keyboardType(.asciiCapableNumberPad)
//                    .keyboardType(.default)
//                    .keyboardType(.namePhonePad)
//                    .keyboardType(.numberPad)
//                    .keyboardType(.numbersAndPunctuation)
//                    .keyboardType(.phonePad)
//                    .keyboardType(.twitter)
//                    .keyboardType(.webSearch)
                }
                .scrollDismissesKeyboard(.immediately)
            }
            .navigationTitle("Keyboard Type Demo")
        }
    }
}

#Preview {
    KeyboardType()
}
