//
//  Location.swift
//  LearnSwift
//
//  Created by P21_0079 on 16/10/24.
//

import Foundation
import MapKit

struct Location: Equatable, Identifiable {
    var id: UUID = .init()
    var name: String
    var description: String
    var latitude: Double
    var longitude: Double

    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }

    static let example = Location(name: "Kakariya lake", description: "Kakariya lake", latitude: 23.006165363572737, longitude: 72.60117433360871)

    static func == (lhs: Location, rhs: Location) -> Bool {
        lhs.id == rhs.id
    }
}

struct NearByPlacesResult: Codable {
    public let query: Query
}

struct Query: Codable {
    let pages: [Int: Page]
}

struct Page: Codable, Comparable {
    let pageid: Int
    let title: String
    let coordinates: [Coordinate]
    let terms: [String: [String]]?

    // extra properties
    var description: String {
        terms?["description"]?.first ?? "No description available"
    }

    var thumbnailImageURL: URL? {
        if let source = thumbnail?.source {
            return URL(string: source)
        }
        return nil
    }

    let thumbnail: Thumbnail?

    static let example = Page(
        pageid: 1471941,
        title: "Dholka",
        coordinates: [Coordinate(lat: 22.72, lon: 72.47, primary: "", globe: "earth")],
        terms: ["description": ["city in Gujarat state, India"]],
        thumbnail: Thumbnail(source: "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Bahlol_Khan_Gazi%27s_Mosque_02.jpg/500px-Bahlol_Khan_Gazi%27s_Mosque_02.jpg", width: 500, height: 380))

    static func == (lhs: Page, rhs: Page) -> Bool {
        lhs.title < rhs.title
    }

    static func < (lhs: Page, rhs: Page) -> Bool {
        lhs.title < rhs.title
    }
}

struct Coordinate: Codable {
    let lat: Double
    let lon: Double
    let primary: String
    let globe: String
}

struct Thumbnail: Codable {
    let source: String
    let width: Int
    let height: Int
}
