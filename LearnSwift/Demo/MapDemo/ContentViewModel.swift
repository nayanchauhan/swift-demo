//
//  ContentViewModel.swift
//  LearnSwift
//
//  Created by P21_0079 on 10/10/24.
//

import Foundation
import MapKit
    
final class ContentViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    var locationManager: CLLocationManager?
    
    func locationManagerAuthorization(_ manager: CLLocationManager) {
        let previousAuthorizationStatus = manager.authorizationStatus
        
        // request authorization permission
        manager.requestWhenInUseAuthorization()
        
        if manager.authorizationStatus != previousAuthorizationStatus {}
    }
}
