//
//  EditPlaceDetailsView.swift
//  LearnSwift
//
//  Created by P21_0079 on 16/10/24.
//

import SwiftUI

enum LoadingState {
    case loading, loaded, failed
}

struct EditPlaceDetailSheet: View {
    @State private var name: String
    @State private var description: String
    @State private var loadingState = LoadingState.loading
    @State private var nearbyPlaces = [Page]()

    @Environment(\.dismiss) var dismiss

    var location: Location
    var onSave: (Location) -> Void

    init(location: Location, onSave: @escaping (Location) -> Void) {
        self.location = location
        self.onSave = onSave

        _name = State(initialValue: location.name)
        _description = State(initialValue: location.description)
    }

    var body: some View {
        NavigationStack {
            VStack {
                Form {
                    Section {
                        TextField("Place Name", text: $name)
                        TextField("Description", text: $description)
                    }

                    // MARK: Nearby places view

                    Section("Nearby Places") {
                        switch loadingState {
                            case .loading:
                                HStack {
                                    Spacer()
                                    ProgressView("Fetching nearby places...")
                                        .padding()
                                    Spacer()
                                }

                            case .loaded:
                                if nearbyPlaces.isEmpty {
                                    Text("No nearby places found.")
                                } else {
                                    ForEach(nearbyPlaces, id: \.pageid) { place in
                                        NavigationLink {
                                            NearbyPlaceDetail(place: place)
                                        }
                                        label: {
                                            HStack {
                                                if place.thumbnailImageURL != nil {
                                                    AsyncImage(url: place.thumbnailImageURL) { img in
                                                        img
                                                            .resizable()
                                                            .frame(width: 50, height: 40)
                                                            .clipShape(RoundedRectangle(cornerRadius: 5))
                                                    } placeholder: {
                                                        ProgressView()
                                                            .frame(width: 50, height: 40)
                                                    }
                                                } else {
                                                    Image(LearnSwift.ImageName.defaultImageFrame)
                                                        .resizable()
                                                        .frame(width: 50, height: 40)
                                                        .clipShape(RoundedRectangle(cornerRadius: 5))
                                                }

                                                VStack(alignment: .leading) {
                                                    Text(place.title)
                                                    Text(place.description)
                                                        .foregroundStyle(.secondary)
                                                }
                                            }
                                        }
                                    }
                                }

                            case .failed:
                                Text("Something went wrong")
                        }
                    }
                }
//                .scrollIndicators(.hidden)
                .toolbar {
                    ToolbarItem {
                        Button("Save") {
                            var newLocation = location
                            newLocation.name = name
                            newLocation.description = description
                            newLocation.id = UUID()
                            onSave(newLocation)
                            dismiss()
                        }
                    }
                }
                .task { fetchNearbyPlaces() }
            }
        }
    }

    // MARK: Fetching nearby places by latitude and longitude api, Method - GET, Api key not required

    private func fetchNearbyPlaces() {
        let urlString = "https://en.wikipedia.org/w/api.php?ggscoord=\(location.latitude)%7C\(location.longitude)&action=query&prop=coordinates%7Cpageimages%7Cpageterms&colimit=50&piprop=thumbnail&pithumbsize=500&pilimit=50&wbptterms=description&generator=geosearch&ggsradius=10000&ggslimit=50&format=json"

        guard let url = URL(string: urlString) else {
            print("Bad request URL")
            return
        }

        var requestURL = URLRequest(url: url)
        requestURL.httpMethod = "GET"
        requestURL.setValue("Application/json", forHTTPHeaderField: "Content-Type")

        let task = URLSession.shared.dataTask(with: requestURL) { data, _, error in

            if let error = error {
                fatalError("😡 ERROR : \(error)")
            }

            guard let data = data else {
                fatalError("😡 ERROR : \(String(describing: error))")
            }

            // Decode data into json format
            let decoder = JSONDecoder()

            do {
                let apiData = try decoder.decode(NearByPlacesResult.self, from: data)
                nearbyPlaces = apiData.query.pages.values.sorted()
                loadingState = .loaded
            } catch {
                loadingState = .failed
                fatalError("😡 ERROR : \(error)")
            }
        }

        task.resume()
    }
}

#Preview {
    EditPlaceDetailSheet(location: Location.example) { _ in }
}
