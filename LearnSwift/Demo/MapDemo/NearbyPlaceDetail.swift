//
//  NearbyPlaceDetail.swift
//  LearnSwift
//
//  Created by P21_0079 on 17/10/24.
//

import SwiftUI

struct NearbyPlaceDetail: View {
    let place: Page

    var body: some View {
        NavigationStack {
            Form {
                Section("Place Detail") {
                    // MARK: Place Image

                    if place.thumbnailImageURL != nil {
                        AsyncImage(url: place.thumbnailImageURL) { PlaceImage in
                            PlaceImage
                                .resizable()
                                .aspectRatio(contentMode: .fit)

                        } placeholder: {
                            HStack {
                                Spacer()
                                ProgressView("Loading Image...")
                                Spacer()
                            }
                        }
                    } else {
                        Image(LearnSwift.ImageName.defaultImageFrame)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                    }

                    HStack {
                        Text("Name")
                            .font(.title3)
                        Spacer()
                        Text(place.title)
                            .foregroundStyle(.secondary)
                    }

                    HStack {
                        Text("Latitude")
                            .font(.title3)
                        Spacer()
                        Text(String(place.coordinates.first!.lat))
                            .foregroundStyle(.secondary)
                    }

                    HStack {
                        Text("Longitude")
                            .font(.title3)
                        Spacer()
                        Text(String(place.coordinates.first!.lon))
                            .foregroundStyle(.secondary)
                    }

                    VStack(alignment: .leading) {
                        Text("Description")
                            .font(.title3)
                        Text(place.description)
                            .foregroundStyle(.secondary)
                    }
                }
            }
        }
        .navigationTitle(place.title)
        .navigationBarTitleDisplayMode(.inline)
        .task {
            print("place detail", place)
        }
    }
}

#Preview {
    NearbyPlaceDetail(place: Page.example)
}
