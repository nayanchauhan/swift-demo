//
//  MapDemo.swift
//  LearnSwift
//
//  Created by P21_0079 on 10/10/24.
//

import MapKit
import SwiftUI

// MARK: Request url for nearby places

struct MapDemo: View {
    @State private var locations = [Location]()

    @State private var coordinateReigon: MKCoordinateRegion =
        .init(center: CLLocationCoordinate2D(latitude: 23.022505, longitude: 72.571365), span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta:
            0.1))

    @State private var selectedPlace: Location?

    var body: some View {
        NavigationStack {
            if #available(iOS 17.0, *) {
                ZStack {
                    // MARK: Map view

                    Map(coordinateRegion: $coordinateReigon, annotationItems: locations) { location in
                        MapAnnotation(coordinate: location.coordinate) {
                            ZStack {
                                Button {
                                    selectedPlace = location
                                }
                                label: {
                                    VStack {
                                        Image(systemName: "star.circle")
                                            .resizable()
                                            .aspectRatio(contentMode: .fit)
                                            .frame(width: 40, height: 40)
                                            .foregroundStyle(.red)
                                    }
                                }
                                Text(location.name)
                                    .fixedSize()
                                    .foregroundStyle(.white)
                                    .font(.subheadline)
                                    .padding(EdgeInsets(top: 1, leading: 10, bottom: 1, trailing: 10))
                                    .background(.black.opacity(0.5))
                                    .clipShape(Capsule())
                                    .padding(.top, 60)
                            }
                        }
                    }

                    // MARK: Center Dot

                    Image(systemName: "dot.scope")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .foregroundStyle(.indigo)
                        .frame(width: 35, height: 35)

                    // MARK: Add location

                    VStack {
                        Spacer()
                        HStack {
                            Spacer()
                            Button { addLocation() }
                                label: {
                                    Image(systemName: "plus")
                                        .resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(width: 25, height: 25)
                                        .foregroundStyle(.black.opacity(0.7))
                                        .padding(10)
                                        .background(.regularMaterial)
                                        .clipShape(Circle())
                                        .shadow(radius: 2)
                                }
                        }
                        .padding()
                    }
                }
                .sheet(item: $selectedPlace) { place in
                    EditPlaceDetailSheet(location: place) { newLocation in
                        let index = locations.firstIndex(of: place)!
                        locations[index] = newLocation
                    }
                }
            } else {
                // below 17.0 ios version
            }
        }
    }

    func addLocation() {
        locations.append(
            Location(name: "Test Location",
                     description: "Test Descrition",
                     latitude: coordinateReigon.center.latitude,
                     longitude: coordinateReigon.center.longitude)
        )
    }
}

#Preview {
    MapDemo()
}
