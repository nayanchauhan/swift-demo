//
//  HapticFeedback.swift
//  LearnSwift
//
//  Created by P21_0079 on 28/10/24.
//

import CoreHaptics
import SwiftUI

struct HapticFeedback: View {
    @State private var engine: CHHapticEngine?

    var body: some View {
        VStack {
            Button("Success") {
                hapticFeedbackGenerator(type: .success)
            }
            .buttonStyle(.borderedProminent)

            Button("Error") {
                hapticFeedbackGenerator(type: .error)
            }
            .buttonStyle(.borderedProminent)

            Button("Warning") {
                hapticFeedbackGenerator(type: .warning)
            }
            .buttonStyle(.borderedProminent)
        }
    }

    func hapticFeedbackGenerator(type: UINotificationFeedbackGenerator.FeedbackType) {
        let generator = UINotificationFeedbackGenerator()

        generator.notificationOccurred(type)
    }
}

#Preview {
    HapticFeedback()
}
