//
//  LoadVideoAsynchronusly.swift
//  LearnSwift
//
//  Created by P21_0079 on 25/11/24.
//

import AVFoundation
import SwiftUI

struct LoadVideoAsynchronusly: View {
    @State private var thumbnail: Image?

    var body: some View {
        VStack {
            if let thumbnail {
                thumbnail
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            } else {
                Text("No thumbnail available")
            }
        }
        .onAppear {
            Task {
                if let image = await generateImageFromVideo() {
                    print("thumbnail", image)
                    thumbnail = image
                }
            }
        }
    }

    func loadVideo() async {
        guard let vidURL = Bundle.main.url(forResource: "sample_video_2", withExtension: "mp4") else {
            return
        }

        let asset = AVAsset(url: vidURL)

        do {
            let loadTrack = try await asset.load(.metadata)
        } catch {
            print("ERROR: loading video duration")
        }
    }

    func generateImageFromVideo() async -> Image? {
        guard let vidURL = Bundle.main.url(forResource: "sample_video_1", withExtension: "mp4") else { return nil }

        let asset = AVAsset(url: vidURL)

        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.requestedTimeToleranceBefore = .zero
        imageGenerator.requestedTimeToleranceAfter = CMTime(seconds: 2.0, preferredTimescale: 600)

        do {
            let image = try await imageGenerator.image(at: CMTime(seconds: 15.0, preferredTimescale: 600)).image

            let convertToUIImage = UIImage(cgImage: image)
            return Image(uiImage: convertToUIImage)
        } catch {
            print("Error while generating image", error.localizedDescription)
            return nil
        }
    }
}

#Preview {
    LoadVideoAsynchronusly()
}
