//
//  EnviromentObjDemo.swift
//  LearnSwift
//
//  Created by P21_0079 on 18/10/24.
//

import SwiftUI

class TestEnviromentModel: ObservableObject {
    @Published var name = "Narendra Modiji"
}

struct TestViewOne: View {
    @EnvironmentObject var testEnviromentModel: TestEnviromentModel

    var body: some View {
        Text("Test view One : \(testEnviromentModel.name)")
    }
}

struct TestViewTwo: View {
    @EnvironmentObject var testEnviromentModel: TestEnviromentModel

    var body: some View {
        Text("Test view Two : \(testEnviromentModel.name)")
    }
}

struct ChangeName: View {
    @EnvironmentObject var testEnviromentModel: TestEnviromentModel

    var body: some View {
        Form {
            TextField("Name", text: $testEnviromentModel.name)
        }
    }
}

struct EnviromentObjDemo: View {
    @State private var testEnvModel = TestEnviromentModel()

    var body: some View {
        VStack {
            TestViewOne()
                .environmentObject(testEnvModel)
            TestViewTwo()
                .environmentObject(testEnvModel)
            ChangeName()
                .environmentObject(testEnvModel)
        }
    }
}

#Preview {
    EnviromentObjDemo()
}
