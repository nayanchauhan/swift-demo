import AVFoundation
import SwiftUI
import UIKit

// MARK: - Video Player View that supports transparency

class TransparentVideoPlayerView: UIView {
    private var playerLayer: AVPlayerLayer?
    private var player: AVPlayer?
    
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = .clear // Essential for transparency
        playerLayer = layer as? AVPlayerLayer
        playerLayer?.videoGravity = .resizeAspectFill
        playerLayer?.backgroundColor = CGColor(gray: 0, alpha: 0) // Transparent background
    }
    
    func configure(with url: URL) {
        let playerItem = AVPlayerItem(url: url)
        player = AVPlayer(playerItem: playerItem)
        playerLayer?.player = player
        
        // Loop the video
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime,
                                               object: playerItem,
                                               queue: .main)
        { [weak self] _ in
            self?.player?.seek(to: .zero)
            self?.player?.play()
        }
    }
    
    func play() {
        player?.play()
    }
    
    func pause() {
        player?.pause()
    }
    
    func stop() {
        player?.pause()
        player?.seek(to: .zero)
    }
}

// MARK: - SwiftUI Wrapper for the Video Player

struct TransparentVideoPlayer: UIViewRepresentable {
    let videoURL: URL
    let autoPlay: Bool
    
    init(videoURL: URL, autoPlay: Bool = true) {
        self.videoURL = videoURL
        self.autoPlay = autoPlay
    }
    
    func makeUIView(context: Context) -> TransparentVideoPlayerView {
        let view = TransparentVideoPlayerView()
        view.configure(with: videoURL)
        if autoPlay {
            view.play()
        }
        return view
    }
    
    func updateUIView(_ uiView: TransparentVideoPlayerView, context: Context) {
        // Handle updates if needed
    }
}

// MARK: - Example Usage View

struct TransparentVideoView: View {
    @State private var selectedBackground: Color = .blue
    let videoURL: URL // Your video URL with alpha channel
    
    var body: some View {
        ZStack {
            // Background color or pattern
            selectedBackground
                .ignoresSafeArea()
            
            // Video player with transparency
            TransparentVideoPlayer(videoURL: videoURL)
                .aspectRatio(contentMode: .fit)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            
            // Optional controls
            VStack {
                Spacer()
                
                // Background color picker
                HStack {
                    ColorPicker("Background", selection: $selectedBackground)
                        .labelsHidden()
                    
                    Text("Change Background")
                        .foregroundColor(.white)
                }
                .padding()
            }
        }
    }
}

// MARK: - Advanced Version with Controls

struct AdvancedTransparentVideoView: View {
    @State private var selectedBackground: Color = .blue
    @State private var isPlaying: Bool = true
    @State private var showControls: Bool = true
    
    let videoURL: URL
    
    private let backgroundOptions: [(String, Color)] = [
        ("Blue", .blue),
        ("Green", .green),
        ("Black", .black),
        ("White", .white)
    ]
    
    var body: some View {
        ZStack {
            selectedBackground
                .ignoresSafeArea()
            
            TransparentVideoPlayer(videoURL: videoURL, autoPlay: isPlaying)
                .aspectRatio(contentMode: .fit)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            
            if showControls {
                VStack {
                    Spacer()
                    
                    // Background selector
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack(spacing: 15) {
                            ForEach(backgroundOptions, id: \.0) { _, color in
                                Button(action: {
                                    selectedBackground = color
                                }) {
                                    Circle()
                                        .fill(color)
                                        .frame(width: 30, height: 30)
                                        .overlay(
                                            Circle()
                                                .stroke(Color.white, lineWidth: selectedBackground == color ? 2 : 0)
                                        )
                                }
                            }
                        }
                        .padding()
                    }
                    .background(Color.black.opacity(0.5))
                    .cornerRadius(20)
                    .padding()
                }
            }
            
            // Tap gesture to show/hide controls
            Color.clear
                .contentShape(Rectangle())
                .onTapGesture {
                    withAnimation {
                        showControls.toggle()
                    }
                }
        }
        .overlay(
            Button(action: {
                isPlaying.toggle()
            }) {
                Image(systemName: isPlaying ? "pause.circle.fill" : "play.circle.fill")
                    .font(.system(size: 40))
                    .foregroundColor(.white)
                    .shadow(radius: 5)
            }
            .opacity(showControls ? 1 : 0)
            .padding(),
            alignment: .topTrailing
        )
    }
}

#Preview {
    if let videoURL = Bundle.main.url(forResource: "transparent_vid", withExtension: "mp4") {
        AdvancedTransparentVideoView(videoURL: videoURL)
    } else {
        Text("Video not found")
    }
}
