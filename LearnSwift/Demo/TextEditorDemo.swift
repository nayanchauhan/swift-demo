//
//  TextEditorDemo.swift
//  LearnSwift
//
//  Created by P21_0079 on 07/08/24.
//

import SwiftUI

struct TextEditorDemo: View {
    @State private var text: String = ""
    @State private var selectedText: String = ""
    @State private var pastedText = ""
    @State private var name: String = "John Doe"
    @FocusState private var isNameFieldFocus: Bool

    var body: some View {
        NavigationStack {
            VStack {
                Form {
                    Section {
                        Text("""
                        Fira Code Custom Font

                        In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content

                        """, comment: "This is testing comment")
                            .padding()
                            .textSelection(.enabled)
                            .font(Font.FiraCodeRegular(size: 25))
                    }

                    // MARK: - Rename Button Demo

                    Section {
                        HStack {
                            TextField("Your Name", text: $name)
                                .focused($isNameFieldFocus)
                                .contextMenu { RenameButton() }
                                .renameAction { isNameFieldFocus = true }
                        }
                    }

                    // MARK: - how to paste copied text, show paste system button.

                    Section {
                        PasteButton(payloadType: String.self) { pastedText = $0[0] }
                        Text(pastedText)
                    }

                    Section {
                        List(1 ..< 51) {
                            Text("\($0) x \($0) = \($0 * $0)")
                        }
                    }
                }
            }
            .navigationTitle("Text Editor Demo")
            .scrollDismissesKeyboard(.immediately)
        }
    }
}

#Preview {
    TextEditorDemo()
}
