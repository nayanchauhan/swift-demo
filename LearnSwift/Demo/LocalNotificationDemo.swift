//
//  NotificationDemo.swift
//  LearnSwift
//
//  Created by P21_0079 on 09/10/24.
//

import SwiftUI
import UserNotifications

struct LocalNotificationDemo: View {
    let center = UNUserNotificationCenter.current()
    @State private var isNotificationTriggered: Bool = false
    @State private var isPermissionGranted: Bool = false
    @State private var isShowingPermissionAlert: Bool = false
    @State private var isPermissionDenied: Bool = false

    @Environment(\.scenePhase) var scenePhase
    
    var body: some View {
        NavigationStack {
            Form {
                Button("Grant permission") {
                    Task {
                        await askNotificationPermission()
                    }
                }
            
                Button("Trigger Local Notification") {
                    Task {
                        await sendLocalNotification()
                    }
                }
            }
            .navigationTitle("Notification")
            .alert("Success", isPresented: $isNotificationTriggered) {
                Button("Ok", role: .cancel) {}
                Button("Go to background") { exit(0) }
                
            } message: {
                Text("Notification triggered successfully")
            }
            .alert(isPermissionGranted ? "Permission Already Granted" : "Notification permission not provided", isPresented: $isShowingPermissionAlert) {
                if isPermissionDenied {
                    Button("Open Settings") {
                        if let url = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.open(url)
                        }
                    }
                } else {
                    Button("Ok", role: .cancel) {}
                }
        
            } message: {
                if isPermissionGranted {
                    Text("Permision already granted, You can triggered new local notification by clicking on \"Trigger Local Notification\" button")
                } else {
                    Text("Please provide notification permission to trigger new local notifications")
                }
            }
            .onChange(of: scenePhase, perform: { appState in
                if appState == .active {
                    // reset state when app comes from background, in kill mode it automatically reset local state
                    isPermissionDenied = false
                }
            })
        }
    }
    
    private func sendLocalNotification() async {
        let content = UNMutableNotificationContent()
        content.title = "Test Notification Title"
        content.body = "Test Notification Body"
        content.sound = UNNotificationSound.default
        
        let triggerInterval = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let notificationRequest = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: triggerInterval)
        
        let authorizationStatus = await center.notificationSettings().authorizationStatus
        
        if authorizationStatus != .authorized {
            isShowingPermissionAlert = true
            isPermissionGranted = false
            return
        }
        
        do {
            try await center.add(notificationRequest)
            isNotificationTriggered = true
        } catch {
            print("Error occours wile send local notification", error.localizedDescription)
        }
    }
    
    private func askNotificationPermission() async {
        // get user authorization status
        let authorizationStatus = await center.notificationSettings().authorizationStatus
        
        if authorizationStatus == .denied {
            isShowingPermissionAlert = true
            return isPermissionDenied = true
        }
            
        if authorizationStatus == .authorized {
            isShowingPermissionAlert = true
            return isPermissionGranted = true
        }

        do {
            try await center.requestAuthorization(options: [.alert, .sound, .badge])
        } catch {
            print("Error while asking permission", error.localizedDescription)
        }
    }
}

#Preview {
    LocalNotificationDemo()
}
